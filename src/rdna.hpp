#include <string>
#include <random>
#include <iostream>
using namespace std;
using std::string;

string randDNA(int seed, string bases, int n)
{
	mt19937 eng(seed); //engine
	uniform_int_distribution<int> let(0,bases.size() - 1); //uniform distri

	string dnastring; //initializing output
	for(int x = 0; x < n; x++)
	{
		int i;
		i = let(eng);
		dnastring += bases[i];
	}
	return dnastring; 
}
